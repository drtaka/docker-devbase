FROM debian:jessie

# change time zone
RUN mv /etc/localtime /etc/localtime.org
RUN ln -s /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

RUN apt-get update

# genrate and change locale.
RUN apt-get -y install locales
RUN sed -i 's/# \(ja_JP\.UTF-8 UTF-8\)/\1/' /etc/locale.gen
RUN locale-gen
ENV LANG="ja_JP.UTF-8" LC_ALL=$LANG
RUN dpkg-reconfigure -f noninteractive locales

# install emacs
RUN apt-get -y install emacs24-nox
ENV TERM xterm

RUN apt-get -y install build-essential git curl silversearcher-ag

# Ruby
RUN apt-get -y install ruby ruby-dev
RUN gem install bundler rcodetools

# Golang
# [golang/Dockerfile at d7e2a8d90a9b8f5dfd5bcd428e0c33b68c40cc19 · docker-library/golang · GitHub](https://github.com/docker-library/golang/blob/d7e2a8d90a9b8f5dfd5bcd428e0c33b68c40cc19/1.5/Dockerfile)
# gcc for cgo
RUN apt-get install -y --no-install-recommends \
		g++ \
		gcc \
		libc6-dev \
		make \
	&& rm -rf /var/lib/apt/lists/*

ENV GOLANG_VERSION 1.5.4
ENV GOLANG_DOWNLOAD_URL https://golang.org/dl/go$GOLANG_VERSION.linux-amd64.tar.gz
ENV GOLANG_DOWNLOAD_SHA256 a3358721210787dc1e06f5ea1460ae0564f22a0fbd91be9dcd947fb1d19b9560

RUN curl -fsSL "$GOLANG_DOWNLOAD_URL" -o golang.tar.gz \
	&& echo "$GOLANG_DOWNLOAD_SHA256  golang.tar.gz" | sha256sum -c - \
	&& tar -C /usr/local -xzf golang.tar.gz \
	&& rm golang.tar.gz

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"
WORKDIR $GOPATH

COPY go-wrapper /usr/local/bin/

# set up my user
RUN useradd drtaka -u 1000 -s /bin/bash

# set password. please change this later.
RUN echo "root:docker!" | chpasswd
RUN echo "drtaka:docker!" | chpasswd

WORKDIR /home/drtaka
ENV HOME /home/drtaka
RUN chown -R drtaka:drtaka /home/drtaka

VOLUME ["/home"]
USER drtaka
EXPOSE 8081
